#region usings
using System;
using System.Collections.Generic;
using System.Net;
using System.ComponentModel.Composition;

using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
using VVVV.PluginInterfaces.V2.Graph;
using VVVV.Utils.VMath;

using VVVV.Core;
using VVVV.Core.Logging;
#endregion usings
namespace VVVV.Nodes
{
	#region PluginInfo
	[PluginInfo(Name = "GetPinValue",
	Category = "IRIS",
	Help = "", Tags = "",
	AutoEvaluate = true)]
	#endregion PluginInfo
	public class PatchWatch: IDisposable, IPluginEvaluate
	{
		#region fields & pins
		
		[Input("Pin Path", IsSingle = false)]
		IDiffSpread<string> FPinPath;
		
		[Input("Update", IsBang = true)]
		ISpread<bool> FUpdate;
		
		[Output("Output")]
		ISpread<string> FOutput;
		
		[Output("Tags")]
		ISpread<string> FTagOut;
		
		[Output("BinSize")]
		ISpread<int> FOutputBinSize;
		
		[Import()]
		ILogger FLogger;
		
		[Import()]
		IHDEHost FHDEHost;
		
		[Import()]
		IPluginHost FPluginHost;
		
		int nodeID;
		private INode2 FPatch;
		private bool FFirstFrame = true;
		// Track whether Dispose has been called.
		private bool FDisposed = false;
		
		string pinPath;
		string pinName;
		string outputValue;
		private List<string> FInputs = new List<string>();
		private List<string> FPinValues = new List<string>();
		private List<string> FPinTags = new List<string>();
		private List<int> FBinSizes = new List<int>();
		#endregion fields & pins
		#region constructor/destructor
		public PatchWatch()
		{}
		~PatchWatch()
		{
			Dispose(false);
		}
		public void Dispose()
		{
			Dispose(true);
		}
		protected void Dispose(bool disposing)
		{
			FDisposed = true;
		}
		#endregion constructor/destructor
		#region events
		//when a node is removed from the patch
		//and it is one of the observed subpatches
		//then unregister all parameter-pins
		//reregister all parameter-pins
		
		#endregion events
		//called when data for any output pin is requested
		public void Evaluate(int SpreadMax)
		{
			//update can be forced
			if ((FUpdate[0]))
			{
				FPinValues.Clear();
				FBinSizes.Clear();
				FPinTags.Clear();
				
				//// THE NEW ONE //////
				for (int i = 0; i < FPinPath.SliceCount; i++)
				{
					pinPath = FPinPath[i];
					string[] splitPath = pinPath.Split('/');
					if (splitPath.Length > 2 ){
						pinPath ="";
						for (int j = 0; j< splitPath.Length-2; j++){
							if (j < splitPath.Length-3)
							pinPath +=  splitPath[j] + "/";
							else
							pinPath +=  splitPath[j];
						}
						
						pinName = splitPath[splitPath.Length-1];
						nodeID = Convert.ToInt32(splitPath[splitPath.Length-2]);
						
						var node = FHDEHost.GetNodeFromPath(pinPath);
						FPatch = node;
						UpdateAllInputsNew(pinName, nodeID);
						
					}
				}
				FOutput.SliceCount = FPinValues.Count;
				for (int k=0; k< FPinValues.Count; k++){
					FOutput[k] = FPinValues[k];
				}
				FOutputBinSize.SliceCount = FBinSizes.Count;
				for (int k=0; k< FBinSizes.Count; k++){
					FOutputBinSize[k] = FBinSizes[k];
				}
				FTagOut.SliceCount = FPinTags.Count;
				for (int k=0; k< FPinTags.Count; k++){
					FTagOut[k] = FPinTags[k];
				}
			}
			
		}
		
		private void UpdateAllInputsNew(string pinName, int nodeID){
			
			foreach (var node in FPatch){
				if (node.ID == nodeID){
					var pin = node.FindPin(pinName);
					var tag =  node.FindPin("Tag").Spread.Trim('|') ;
					var bins = pin.SliceCount;
					FBinSizes.Add(bins);
					FPinTags.Add(tag);
					
					for (int i = 0; i <pin.SliceCount; i++){
						if (pin[i]== null)
						   FPinValues.Add("");
						else
							FPinValues.Add(pin[i].Trim('|'));
					}
				}
		}
		}
	}
}