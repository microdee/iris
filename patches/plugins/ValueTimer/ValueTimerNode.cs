#region usings
using System;
using System.ComponentModel.Composition;

using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
using VVVV.Utils.VColor;
using VVVV.Utils.VMath;

using VVVV.Core.Logging;
#endregion usings

namespace VVVV.Nodes
{
	#region PluginInfo
	[PluginInfo(Name = "Timer", Category = "Value", Help = "Basic template with one value in/out", Tags = "")]
	#endregion PluginInfo
	public class ValueTimerNode : IPluginEvaluate
	{
		
		
		#region fields & pins
		private double FHostTime;
		private double FOldTime;
		private double FCurrentTime;
		public const double MinTimeStep = 0.0001;
		double frames;
		double hosttime;
		bool firstFrame;
		[Input("Reset", IsBang = true)]
		ISpread<bool> FReset;
		
		[Input("FWD", IsBang = true)]
		ISpread<bool> FFWD;
		
		[Input("BWD", IsBang = true)]
		ISpread<bool> FBWD;
		
		[Input("Pause", IsSingle = true)]
		ISpread<bool> FPause;
		
		[Input("FPS")]
		ISpread<double> FFps;

		[Output("Time")]
		ISpread<double> FTimeOut;
		
		[Output("HostTime")]
		ISpread<double> FHostTimeOut;
		[Output("Frames")]
		ISpread<double> FFramesOut;
		[Output("MasterTime")]
		ISpread<string> FMasterTimeOut;
		[Output("MOD")]
		ISpread<double> FModOut;

		[Import()]
		ILogger FLogger;
		[Import()]
		IPluginHost FHost;
		#endregion fields & pins

		public ValueTimerNode()
		{
			firstFrame = true;
		}
		
		//called when data for any output pin is requested
		public void Evaluate(int SpreadMax)
		{
			FTimeOut.SliceCount = SpreadMax;
			FHostTimeOut.SliceCount = SpreadMax;
			FFramesOut.SliceCount = SpreadMax;
			
			FHost.GetCurrentTime(out FHostTime);
			double deltaTime = (FHostTime - FOldTime);
			if ( !FPause[0] ){
				FCurrentTime += deltaTime;
			}
			if (FFWD[0])
				FCurrentTime += 1/FFps[0];
			if (FBWD[0] && (FCurrentTime / (1/FFps[0])) > 0)
				FCurrentTime -= 1/FFps[0];
			
			FOldTime = FHostTime;
			
			if (FReset[0] || firstFrame)
			{
				FCurrentTime = 0;
				firstFrame = false;
				frames = 1;
			}
			
			if (FCurrentTime<0)
				FCurrentTime =0;
			FTimeOut[0] = FCurrentTime;
			FHostTimeOut[0] = FHostTime;
			
			FFramesOut[0] = FCurrentTime / (1/FFps[0]);
	
			string hours = ((int)((FCurrentTime/60)/60)).ToString("D2");
			string minutes = ((int)(FCurrentTime/60)% 60).ToString("D2");
			string seconds = ((int)FCurrentTime % 60).ToString("D2");
			string frame = ((int)((FCurrentTime / (1/FFps[0])) % FFps[0])).ToString("D2");
			FMasterTimeOut[0] = (hours + ":" + minutes + ":" + seconds + ":" + frame);
			//FLogger.Log(LogType.Debug, "hi tty!");
		}
	}
}
