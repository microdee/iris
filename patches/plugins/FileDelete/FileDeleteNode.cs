#region usings
using System;
using System.ComponentModel.Composition;
using System.IO;

using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
using VVVV.Utils.VColor;
using VVVV.Utils.VMath;

using VVVV.Core.Logging;
#endregion usings

namespace VVVV.Nodes
{
	#region PluginInfo
	[PluginInfo(Name = "Delete", 
				Category = "File",
				Help = "Deletes given files",
				Author = "bjo:rn",
				AutoEvaluate = true)]
	#endregion PluginInfo
	public class FileDeleteNode : IPluginEvaluate
	{
		#region fields & pins
		[Input("Input", DefaultString = "file.foo")]
		IDiffSpread<string> FInput;
		
		[Input("Delete", IsBang = true )]
		IDiffSpread<bool> FDelete;
		
		[Output("Deleted")]
		ISpread<bool> FDeleted;
		
		[Output("Status")]
		ISpread<string> FStatus;
	
		[Import()]
		ILogger FLogger;
		#endregion fields & pins
		
		//called when data for any output pin is requested
		public void Evaluate(int SpreadMax)
		{
			
			FDeleted.SliceCount = SpreadMax;
			FStatus.SliceCount = SpreadMax;
			
			
			if(FInput.IsChanged)
			{
				for (int i = 0; i < SpreadMax; i++)
				{
					FStatus[i] = "waiting for deletion"; 
					FDeleted[i] = false;
				}
				
			}
			
			
			if (FDelete.IsChanged)
			{
				for (int i = 0; i < SpreadMax; i++)
				{
					
					if (FDelete[i])
					{
						if (File.Exists(FInput[i]))
						{
							try
							{
								File.Delete(FInput[i]);
								FDeleted[i] = true;
								FStatus[i] = FInput[i] + " has been deleted";
								
							}
							catch (Exception e)
							{
								FStatus[i] = e.ToString();
							}
						
						}
						else FStatus[i] = FInput[i] + " does not exist";
					
					}
				}
			}
			
			
		}
	}
}
