#region usings
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;

using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
using VVVV.PluginInterfaces.V2.Graph;
using VVVV.Utils.VColor;
using VVVV.Utils.VMath;


using VVVV.Core.Logging;
#endregion usings

namespace VVVV.Nodes
{
	#region PluginInfo
	[PluginInfo(Name = "PatchTree", Category = "String", Help = "Basic template with one string in/out", Tags = "")]
	#endregion PluginInfo
	public class StringPatchTreeNode : IPluginEvaluate
	{
		#region fields & pins
		private INode2 FPatch;
		private Dictionary<string, INode2> FNodes = new Dictionary<string, INode2>();
		private bool FFirstFrame = true;
		
		
		[Input("Input", DefaultString = "hello c#")]
		ISpread<string> FInput;
		[Input("Update", IsBang = true)]
		ISpread<bool> FUpdate;
		
		[Output("Output")]
		ISpread<string> FOutput;

		[Import()]
		ILogger FLogger;
		
		[Import()]
		IHDEHost FHDEHost;
		#endregion fields & pins

		//called when data for any output pin is requested
		public void Evaluate(int SpreadMax)
		{
			if (FUpdate[0] || FFirstFrame){
				FNodes.Clear();
				var node = FHDEHost.GetNodeFromPath(nodePath);
				//var node = FH
			}
			FOutput.SliceCount = SpreadMax;
			
			for (int i = 0; i < SpreadMax; i++)
				FOutput[i] = FInput[i].Replace("c#", "vvvv");

			//FLogger.Log(LogType.Debug, "Logging to Renderer (TTY)");
		}
	}
}
