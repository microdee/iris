#region usings
using System;
using System.ComponentModel.Composition;
using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
using VVVV.Utils.VColor;
using VVVV.Utils.VMath;
using System.Windows.Forms;

using VVVV.Core.Logging;
#endregion usings

namespace VVVV.Nodes
{
	#region PluginInfo
	[PluginInfo(Name = "MakeDir", Category = "File", Help = "Basic template with one string in/out", Tags = "", AutoEvaluate = true)]
	#endregion PluginInfo
	public class FileMakeDirNode : IPluginEvaluate
	{
		#region fields & pins

		
		[Input("Root Path", DefaultString = "")]
		ISpread<string> FRootPath;
		
		[Input("Path", DefaultString = "")]
		ISpread<string> FDir;

		[Input("DoCreate", IsBang = true, IsSingle = true)]
		ISpread<bool> FCreate;

		[Import()]
		ILogger FLogger;
		#endregion fields & pins

		//called when data for any output pin is requested
		public void Evaluate(int SpreadMax)
		{
			if (FCreate[0])
			{
				FLogger.Log(LogType.Debug, "hier");
				for (int i=0; i< FDir.SliceCount; i++)
				{
					//Create a new subfolder under the current active folder
					string newPath = System.IO.Path.Combine(FRootPath[i], FDir[i]);
					FLogger.Log(LogType.Debug, newPath);
					// Create the subfolder
					System.IO.Directory.CreateDirectory(newPath);
				}
				
			}
			//FLogger.Log(LogType.Debug, "Logging to Renderer (TTY)");
		}
		
		private void makeDir()
		{	
			
			
		}
			
		
	}
}
