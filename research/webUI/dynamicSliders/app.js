var express = require('express')
  , routes = require('./routes')
  , config = require('./config')
  , osc = require('node-osc')

var module_elements = config.init_elements;
var sliderdata = [];
var slideroscnames = [];
var app = module.exports = express.createServer();

var oscClient = new osc.Client('10.0.1.35', 5000);
var oscServer = new osc.Server(4000, '0.0.0.0');

// Configuration

app.configure(function(){
  app.set('adminpass', config.adminpass);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.set('view options', { pretty: true });
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true })); 
});

app.configure('production', function(){
  app.use(express.errorHandler()); 
});

// Routes

app.get('/', routes.index);
app.get('/iris', routes.iris);

var io=require('socket.io').listen(app, { log: false });
app.listen(3000);
io.sockets.on('connection', function( socket ) {
	
	socket.emit('elements', module_elements);
	
	socket.on('slidervaluechanged', function (data) {

		sliderdata[data.id] = data.val;
		
		console.log('slider ' + data.id + ' changed to ' + data.val);
		console.log('osc name = ' + slideroscnames[data.id]);
		
		socket.broadcast.emit("changevalue", data);
		
		if (slideroscnames[data.id])
			oscClient.send(slideroscnames[data.id], data.val);
	
	});
	
	oscServer.on("message", function (msg, rinfo) {
		var message = [];
		message = msg[2];
		console.log('------------------');		
		console.log(msg);

		for (var i=0; i<message.length; i++) {
			//console.log(i + ": " + message[i]);
			var id = message[i].split("|")[0];
			//console.log(id);
		}
		
		switch (message[0]) {
			case "/exposedpins":
				module_elements.length = 0;
				slideroscnames.length = 0;
				for (var prop in message) {
				  if (message.hasOwnProperty(prop) && prop >= 1) {
				    //console.log("prop: " + prop + " value: " + message[prop]);
					//console.log(message[prop]);
					//var id = message[prop].replace('/', '-');
					//console.log(id);
					if (message[prop] != 'none') {
						module_elements.push('slider' + prop);
						slideroscnames['slider' + prop] = message[prop];
						console.log(slideroscnames['slider' + prop]);
						console.log('sliders count = ' + slideroscnames.length);
						
					}
				  }
				}
				socket.emit('elements', module_elements);
				break;
			default:
				break;
		}
				
		
		

		
		
		for (var prop in message) {
		  if (message.hasOwnProperty(prop) && prop >= 1) {
			for (var slidername in slideroscnames) {
				console.log(slidername + ' = ' + slideroscnames[slidername]);
				if (message[0] == slideroscnames[slidername]) {
					console.log(slidername + ' = ' + message[prop]);
					socket.emit('changevalue', {
						'id': slidername, 
						'val': message[prop]
					});
				}
			}
			/*
			console.log(message[0]);
			console.log(' = ' + message[prop]);
			//console.log(slideroscnames['slider' + prop]);
			for (var slidername in slideroscnames) {
				console.log('slidername = ' + slidername);
			}
			*/
		  }
		}
		

		/*
		module_elements.length = 0;
		console.log(module_elements);
		var i = 0;
		for (var prop in msg) {
		  if (msg.hasOwnProperty(prop)) {
		    console.log("prop: " + prop + " value: " + msg[prop]);
			if (i>1 && msg[prop] != '') module_elements[i] = msg[prop];
			i++;
		  }
		}
		console.log(module_elements);
		console.log("\n\n");
		
		
		socket.emit('elements', module_elements);
		*/
		
	});

	
});
console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
