var modules = [
  { name: 'SignalTest (IRIS.Signal)', email: 'tj@vision-media.ca' },
  { name: 'DefaultSettings (IRIS.Config)', email: 'tobi@vision-media.ca' },
  { name: 'OnMyWay (EX9.Texture Source)', email: 'tobi@vision-media.ca' }
];

var patch = require('../patch');



exports.load = function(req, res, next){
  var id = req.params.id;
  console.log(id);
  req.module = modules[id];
  if (req.module) {
    next();
  } else {
    next(new Error('cannot find module ' + id));
  }
};

exports.view = function(req, res){
  res.render('module', {
    title: req.module.name,
	pins: patch.pins
    //user: req.user
  });
};