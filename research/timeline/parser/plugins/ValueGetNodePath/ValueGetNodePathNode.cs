#region usings
using System;
using System.ComponentModel.Composition;

using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
using VVVV.Utils.VColor;
using VVVV.Utils.VMath;

using VVVV.Core.Logging;
#endregion usings

namespace VVVV.Nodes
{
	#region PluginInfo
	[PluginInfo(Name = "GetNodePath", Category = "Value", Help = "Returns this Node's path in the vvvv graph", Tags = "Node, Path, aivenhoe", Author = "aivenhoe")]
	#endregion PluginInfo
	
	public class GetNodePath : IPluginEvaluate
	{
		#region fields & pins
		[Input("UseDescriptiveName")]
		ISpread<bool> FUseDescriptiveName;

		[Output("NodePath")]
		ISpread<string> FNodePath;
		
		[Output("PatchPath")]
		ISpread<string> FPatchPath;

		[Import()]
		IPluginHost FHost;
		
		#endregion fields & pins
		string sNodePath;
		string sPatchPath="";
		char[] splitchar = { '/' };
		public void Evaluate(int SpreadMax)
		{	
			
			FHost.GetNodePath(FUseDescriptiveName[0], out sNodePath);
			string[] parts = sNodePath.Split(splitchar);
			sPatchPath="";
			for (int i=1; i<parts.Length-1; i++)
				sPatchPath += "/"+parts[i];
			
			FNodePath[0] = sNodePath;
			FPatchPath[0] =  sPatchPath;
			
		}
	}
}
